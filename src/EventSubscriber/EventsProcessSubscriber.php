<?php

namespace Drupal\event_scheduler\EventSubscriber;

use Drupal\Component\Datetime\Time;
use Drupal\Core\State\StateInterface;
use Drupal\event_scheduler\Event\EventScheduleInterface;
use Drupal\event_scheduler\EventSchedulerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\event_scheduler\EventSchedulerDispatcher;
use Drupal\event_scheduler\EventSchedulerDatabaseInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EventsProcessSubscriber.
 */
class EventsProcessSubscriber implements EventSubscriberInterface {

  /**
   * Name of the lock we're using.
   */
  protected const LOCK = 'event-scheduler-process-lock';

  /**
   * @var EventSchedulerDispatcher
   */
  protected $scheduler;

  /**
   * @var EventSchedulerDatabaseInterface
   */
  protected $schedulerDatabase;

  /**
   * @var Time
   */
  protected $time;

  /**
   * @var StateInterface
   */
  protected $state;

  /**
   * @var EventSchedulerInterface
   */
  protected $eventScheduler;

  /**
   * Constructs a new EventsProcessSubscriber object.
   *
   * @param EventSchedulerDispatcher $scheduler
   * @param EventSchedulerInterface $eventScheduler
   * @param EventSchedulerDatabaseInterface $schedulerDatabase
   * @param Time $time
   * @param StateInterface $state
   */
  public function __construct(EventSchedulerDispatcher $scheduler,
                              EventSchedulerInterface $eventScheduler,
                              EventSchedulerDatabaseInterface $schedulerDatabase,
                              Time $time, StateInterface $state) {
    $this->scheduler = $scheduler;
    $this->eventScheduler = $eventScheduler;
    $this->schedulerDatabase = $schedulerDatabase;
    $this->time = $time;
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  static function getSubscribedEvents() {
    // Make sure this happens before the processing of local events.
    $events[KernelEvents::TERMINATE] = ['onKernelTerminate', 1100];

    return $events;
  }

  /**
   * This method is called whenever the KERNEL::TERMINATE event is
   * dispatched.
   *
   * @param Event $event
   */
  public function onKernelTerminate($event) {
    if ($this->state->get('event-scheduler-process-lock', FALSE)) {
      // If this is locked then it's already being processed.
      return;
    }

    // Okay, let's do this thing. Prevent other processes from having a look in.
    $this->state->delete('event-scheduler-process-lock');
    $this->state->set('event-scheduler-process-lock', TRUE);

    // Time now...
    $now = $this->time->getCurrentTime();

    // Fetch the timestamp of the next scheduled event, if this is
    // zero it means there aren't any, and if the timestamp is in
    // the future we don't want to do anything right now.
    $timestamp = $this->schedulerDatabase->nextScheduledEventTimestamp();
    if (!$timestamp || $timestamp > $now) {
      // Make sure to remove the lock...
      $this->state->delete('event-scheduler-process-lock');
      return;
    }

    // Find all scheduled events that have passed their
    // deadline but are not already being processed.
    $conditions = [
      'launch' => ['value' => $now, 'op' => '<'],
      'processed' => ['value' => 0],
    ];

    /** @var EventScheduleInterface | Event $scheduledEvent */
    foreach ($this->eventScheduler->loadEvent($conditions) as $scheduledEvent) {
      // Extract the event's name so we can dispatch it...
      $eventName = $scheduledEvent->getName();
      // And dispatch it normally.
      $this->scheduler->dispatch($eventName, $scheduledEvent);
    }

    // Now delete them.
    $this->schedulerDatabase->delete($conditions);

    // And remove the lock.
    $this->state->delete('event-scheduler-process-lock');
  }

}
