<?php

namespace Drupal\event_scheduler;

use Drupal\event_scheduler\Event\EventScheduleInterface;

/**
 * Interface ScheduledEventsServiceInterface.
 */
interface EventSchedulerInterface {

  /**
   * @param string $eventName
   * @param EventScheduleInterface $event
   */
  public function saveEvent($eventName, EventScheduleInterface $event);

  /**
   * Extract the matching events from the DB, unserializing the event structure.
   *
   * @param array $conditions
   *
   * @param bool $firstOnly
   *
   * @return EventScheduleInterface[] | EventScheduleInterface
   *
   */
  public function loadEvent(array $conditions = [], $firstOnly = FALSE);

  /**
   * Delete the matching events from the DB.
   *
   * @param array $conditions
   *
   */
  public function deleteEvent(array $conditions = []);

  /**
   * @return EventSchedulerDatabase
   */
  public function getDatabase();


}
