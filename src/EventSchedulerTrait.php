<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 23/08/18
 * Time: 14:35
 */

namespace Drupal\event_scheduler;

use Drupal\event_scheduler\Event\EventScheduleInterface;

trait EventSchedulerTrait {

  /**
   * @var int
   */
  protected $id = 0;

  /**
   * @var string
   */
  protected $name;

  /**
   * @var string
   */
  protected $class;

  /**
   * @var string
   */
  protected $tag;

  /**
   * @var int timestamp
   */
  protected $launch;

  /**
   * @var bool
   */
  protected $processed;

  public function initialise(int $launch, string $tag = ''): EventScheduleInterface {
    return $this
      ->setLaunch($launch)
      ->setTag($tag)
      ->setName(static::NAME)
      ->setClass(get_class($this));
  }

  /**
   * @param int $id
   *
   * @return $this
   */
  public function setId($id) {
    $this->id = $id;
    return $this;
  }

  /**
   * @return int
   */
  public function id() {
    return $this->id;
  }

  /**
   * @param string $name
   *
   * @return $this
   */
  public function setName($name) {
    $this->name = $name;
    return $this;
  }

  /**
   * @return string
   */
  public function getName() {
    return $this->name;
  }

  /**
   * @param string $event_class
   *
   * @return $this
   */
  public function setClass($event_class) {
    $this->class = $event_class;
    return $this;
  }

  /**
   * @return string
   */
  public function getClass() {
    return $this->class;
  }

  /**
   * @param string $tag
   *
   * @return $this
   */
  public function setTag($tag) {
    $this->tag = $tag;
    return $this;
  }

  /**
   * @return string
   */
  public function getTag() {
    return $this->tag;
  }

  /**
   * @param int $launch UTC timestamp
   *
   * @return $this
   */
  public function setLaunch($launch) {
    $this->launch = $launch;
    return $this;
  }

  /**
   * @return int UTC timestamp
   */
  public function getLaunch() {
    return $this->launch;
  }

  /**
   * @param bool
   *
   * @return $this
   */
  public function setProcessed($processed = TRUE) {
    $this->processed = $processed;
    return $this;
  }

  /**
   * @return bool
   */
  public function getProcessed() {
    return $this->processed;
  }

}