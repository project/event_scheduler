<?php
/**
 * Created by PhpStorm.
 * User: steve
 * Date: 23/08/18
 * Time: 09:37
 */

namespace Drupal\event_scheduler\Event;

/**
 * Interface EventScheduleInterface
 *
 * @package Drupal\event_scheduler\Event
 */
interface EventScheduleInterface {

  /**
   * Initialise the launch time and tag for this event.
   *
   * @param int $launch
   * @param string $tag
   *
   * @return EventScheduleInterface
   */
  public function initialise(int $launch, string $tag = ''): EventScheduleInterface;

  /**
   * @param int $id
   *
   * @return $this
   */
  public function setId($id);

  /**
   * @return int
   */
  public function id();

  /**
   * @param string $name
   *
   * @return $this
   */
  public function setName($name);

  /**
   * @return string
   */
  public function getName();

  /**
   * @param string $event_class
   *
   * @return $this
   */
  public function setClass($event_class);

  /**
   * @return string
   */
  public function getClass();

  /**
   * @param string $tag
   *
   * @return $this
   */
  public function setTag($tag);

  /**
   * @return string
   */
  public function getTag();

  /**
   * @param int $when UTC timestamp
   *
   * @return $this
   */
  public function setLaunch($when);

  /**
   * @return int UTC timestamp
   */
  public function getLaunch();

  /**
   * @param bool
   *
   * @return $this
   */
  public function setProcessed($processed = TRUE);

  /**
   * @return bool
   */
  public function getProcessed();

}