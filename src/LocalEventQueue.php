<?php

namespace Drupal\event_scheduler;

use Drupal\Component\EventDispatcher\Event;

/**
 * Class LocalEventQueue.
 */
class LocalEventQueue implements LocalEventQueueInterface {

  /**
   * @var Event[]
   */
  protected $queue = [];

  /**
   * Add an event to the queue. We don't use the event name
   * as a key because we might get many events with the same
   * name.
   *
   * @param string $eventName
   * @param Event $event
   */
  public function add($eventName, $event) {
    $this->queue[] = [$eventName, $event];
  }

  /**
   * Returns the queued events as long as there are events to return.
   *
   * @return \Generator
   */
  public function get() {
    while (!empty($this->queue)) {
      [$eventName, $event] = array_shift($this->queue);
      yield $eventName => $event;
    }
  }

}
