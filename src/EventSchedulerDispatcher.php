<?php

namespace Drupal\event_scheduler;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\event_scheduler\Event\EventDelayInterface;
use Drupal\event_scheduler\Event\EventScheduleInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Class EventSchedulerDispatcher.
 *
 * Concept stolen from https://thomas.jarrand.fr/blog/events-part-3/
 */
class EventSchedulerDispatcher implements EventDispatcherInterface, EventSubscriberInterface {

  use LoggerChannelTrait;


  const QUEUE_NAME = 'cron_event_scheduler';

  /**
   * Symfony\Component\EventDispatcher\EventDispatcherInterface definition.
   *
   * @var EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * Queued events
   *
   * @var array
   */
  protected $queue;

  /**
   * Is the dispatcher pageSent to dispatch events?
   *
   * @var boolean
   */
  protected $pageSent;
  
  /**
   * @var EventSchedulerInterface
   */
  protected $scheduler;
  
  /**
   * @var TimeInterface
   */
  protected $time;

  /**
   * The event queue service allows more events to be added to the
   * end while it's being processed, to ensure we don't process
   * events within events (which causes issues when handling entities).
   *
   * @var LocalEventQueueInterface
   */
  protected $localEventQueue;

  /**
   * Constructs a new DelayedEventDispatcher object.
   *
   * @param EventDispatcherInterface $event_dispatcher
   * @param EventSchedulerInterface $scheduler
   * @param TimeInterface $time
   * @param LocalEventQueueInterface $eventQueue
   */
  public function __construct(EventDispatcherInterface $event_dispatcher,
                              EventSchedulerInterface $scheduler,
                              TimeInterface $time,
                              LocalEventQueueInterface $eventQueue) {
    $this->eventDispatcher = $event_dispatcher;
    $this->scheduler = $scheduler;
    $this->time      = $time;
    $this->localEventQueue = $eventQueue;

    $this->pageSent  = FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [KernelEvents::TERMINATE => ['setPageSent', 1000]];
  }

  /**
   * {@inheritdoc}
   */
  public function dispatch($eventName, $event = null) {
    // Is this an event to be scheduled, delayed, or just dispatched?
    if ($event instanceof EventDelayInterface) {
      $this->addToLocalQueue($eventName, $event);
    }
    elseif ($event instanceof EventScheduleInterface) {
      $this->saveToDB($eventName, $event);
    }
    else {
      // It's just an ordinary event, dispatch it normally.
      $this->eventDispatcher->dispatch($eventName, $event);
    }
    return $event;
  }

  /**
   * Add the event to the local queue unless we're already
   * in the terminate phase, in which case dispatch it now.
   *
   * @param string $eventName
   * @param Event $event
   */
  protected function addToLocalQueue($eventName, $event) {
    // To be performed at the end of this scheduled page.
    $this->getLogger('event_scheduler.dispatch')->debug('Queuing locally: ' . $eventName);
    // Haven't reached the end yet, so save it in the local queue.
    $this->localEventQueue->add($eventName, $event);
  }

  /**
   * It's an event that needs to be saved for the future, unless launch time is
   * passed, in which case send it to the local queue (which might dispatch it
   * now, anyway).
   *
   * @param string $eventName
   * @param Event | EventScheduleInterface $event
   */
  protected function saveToDB($eventName, $event) {
    if ($event->getLaunch() > $this->time->getRequestTime()) {
      $this->getLogger('event_scheduler.dispatch')->debug('Saving event: ' . $eventName);
      $this->scheduler->saveEvent($eventName, $event);
    }
    else {
      // This should be executed now, so add it to the queue.
      $this->getLogger('event_scheduler.dispatch')->debug('Queuing locally (because overdue): ' . $eventName);
      $this->addToLocalQueue($eventName, $event);
    }
  }

  /**
   * Set pageSent flag to avoid recursion, and
   * dispatch locally queued events for processing.
   */
  public function setPageSent() {
    if (!$this->pageSent) {
      $this->pageSent = TRUE;

      foreach ($this->localEventQueue->get() as $eventName => $event) {
        $this->getLogger('event_scheduler.dispatch')->debug("Dispatching locally queued event: {$eventName}");
        $this->eventDispatcher->dispatch($eventName, $event);
      }
    }
  }

  //=================================================== STANDARD METHODS

  /**
   * Adds an event listener that listens on the specified events.
   *
   * @param string $eventName The event to listen on
   * @param callable $listener The listener
   * @param int $priority The higher this value, the earlier an event
   *                            listener will be triggered in the chain (defaults to 0)
   */
  public function addListener($eventName, $listener, $priority = 0) {
    $this->eventDispatcher->addListener($eventName, $listener, $priority);
  }

  /**
   * Adds an event subscriber.
   *
   * The subscriber is asked for all the events he is
   * interested in and added as a listener for these events.
   *
   * @param EventSubscriberInterface $subscriber
   */
  public function addSubscriber(EventSubscriberInterface $subscriber) {
    $this->eventDispatcher->addSubscriber($subscriber);
  }

  /**
   * Removes an event listener from the specified events.
   *
   * @param string $eventName The event to remove a listener from
   * @param callable $listener The listener to remove
   */
  public function removeListener($eventName, $listener) {
    $this->eventDispatcher->removeListener($eventName, $listener);
  }

  /**
   * Removes an event subscriber from the specified events.
   *
   * @param EventSubscriberInterface $subscriber
   */
  public function removeSubscriber(EventSubscriberInterface $subscriber) {
    $this->eventDispatcher->removeSubscriber($subscriber);
  }

  /**
   * Gets the listeners of a specific event or all listeners sorted by descending priority.
   *
   * @param string $eventName The name of the event
   *
   * @return array The event listeners for the specified event, or all event listeners by event name
   */
  public function getListeners($eventName = NULL) {
    return $this->eventDispatcher->getListeners($eventName);
  }

  /**
   * Gets the listener priority for a specific event.
   *
   * Returns null if the event or the listener does not exist.
   *
   * @param string $eventName The name of the event
   * @param callable $listener The listener
   *
   * @return int|null The event listener priority
   */
  public function getListenerPriority($eventName, $listener) {
    return $this->eventDispatcher->getListenerPriority($eventName, $listener);
  }

  /**
   * Checks whether an event has any registered listeners.
   *
   * @param string $eventName The name of the event
   *
   * @return bool true if the specified event has any listeners, false otherwise
   */
  public function hasListeners($eventName = NULL) {
    return $this->eventDispatcher->hasListeners($eventName);
  }
}