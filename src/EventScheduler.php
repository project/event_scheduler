<?php

namespace Drupal\event_scheduler;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\event_scheduler\Event\EventScheduleInterface;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class ScheduledEventsService.
 */
class EventScheduler implements EventSchedulerInterface {

  const FORMAT = 'json';

  /**
   * @var EventSchedulerDatabase
   */
  protected $database;

  /**
   * @var SerializerInterface
   */
  protected $serializer;

  /**
   * @var LoggerChannelInterface
   */
  protected $logger;

  /**
   * Constructs a new ScheduledEventsService object.
   *
   * @param EventSchedulerDatabase $database
   * @param SerializerInterface $serializer
   * @param LoggerChannelFactoryInterface $loggerFactory
   */
  public function __construct(EventSchedulerDatabase $database, SerializerInterface $serializer, LoggerChannelFactoryInterface $loggerFactory) {
    $this->serializer = $serializer;
    $this->logger = $loggerFactory->get('event_scheduler');
    $this->database = $database;
  }

  //---------------------------------------------------------- EVENT OPERATIONS

  /**
   * Insert the event into the DB, serializing the event structure.
   *
   * @param string $eventName
   * @param EventScheduleInterface $event
   *
   * @return int|null
   *
   * @throws \Exception
   */
  public function saveEvent($eventName, EventScheduleInterface $event) {
    $this->logger->debug('Saving event: ' . $eventName);
    /** @var EventScheduleInterface $event */
    $entry =[
      'name' => $eventName,
      'class' => $event->getClass(),
      'tag' => $event->getTag(),
      'event' => $this->serializer->serialize($event, static::FORMAT),
      'launch' => $event->getLaunch(),
      'processed' => 0,
    ];
    return $this->database->insert($entry);
  }

  /**
   * Extract the matching events from the DB, unserializing the event structure.
   *
   * @param array $conditions
   *
   * @param bool $firstOnly
   *
   * @return EventScheduleInterface[] | EventScheduleInterface | null
   *
   */
  public function loadEvent(array $conditions = [], $firstOnly = FALSE) {
    $events = [];
    /** @var \stdClass $values */
    foreach ($this->database->load($conditions) as $values) {
      $this->logger->debug('Loading event: ' . $values->name);
      /** @var EventScheduleInterface $event */
      if ($event = $this->serializer->deserialize($values->event, $values->class, static::FORMAT)) {
        $this->logger->debug('Deserialized event: ' . $values->name);
        $event->setId($values->id);

        $events[$event->id()] = $event;
      }
    }
    return empty($events) ? ($firstOnly ? NULL : []) : ($firstOnly ? reset($events) : $events);
  }

  /**
   * Delete the matching events from the DB.
   *
   * @param array $conditions
   *
   */
  public function deleteEvent(array $conditions = []) {
    $this->database->delete($conditions);
  }

  /**
   * @return EventSchedulerDatabase
   */
  public function getDatabase() {
    return $this->database;
  }

}
